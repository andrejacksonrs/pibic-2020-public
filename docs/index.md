# Iniciação Científica : Molhamento em superfícies heterogêneas

<img src="images/dropsubstrate.jpg" alt="dropsubstrate" width=40% /><img src="images/Cubaud2003.jpg" alt="Cubaud2003" width=50% />

Quando dois materiais estão em contato, entre eles há uma área limitada por um determinado contorno. Esse contorno é conhecido como linha de contato. O molhamento está relacionado com a dinâmica da área de contato entre um fluido e um sólido [1] (Veja as imagens acima), que é governada pela interação intermolecular entre os meios sólido, líquido e gasoso que compõem o sistema físico estudado [2].

As propriedades de molhamento de fluidos simples e complexos são relevantes para uma gama enorme de aplicações incluindo antibiofouling [[3]](https://en.wikipedia.org/wiki/Biofouling) e revestimentos hidrofóbicos [[4]](https://en.wikipedia.org/wiki/Ultrahydrophobicity), curativos [[5]](https://en.wikipedia.org/wiki/Dressing_(medical)), coletores de água [[6]](https://en.wikipedia.org/wiki/Atmospheric_water_generator), e sistemas nano-eletromecânicos [[7]](https://en.wikipedia.org/wiki/Nanoelectromechanical_systems). Muitos fenômenos no equilíbrio podem ser explicados usando uma abordagem macroscópica na qual a forma do líquido resulta de um princípio de minimização da superfície. Entretanto, não há muitas explicações a cerca de quando o sistema sai do equilíbrio, se movimentando em um substrato, por exemplo. A linha de contato pode ser submetida a mecanismos complexos de dissipação quando o fluido se move e através de instabilidades incluindo stick-slip (que é um fenômeno associado a quando um objeto "escapole" após uma fricção), avalanches, cavitação, pearling (fenômeno associado a geração de bolhas em aquários, por exemplo). Muitas questões em aberto aparecem quando se trata de processos dissipativos em regimes dinâmicos (regimes com o sistema em movimento).



## Bolsa 1 : Abordagem numérica com Surface Evolver

Surface Evolver é um software interativo livre e open-source para o estudo da forma da superfície de um líquido através de métodos de minimização. A interface líquido-ar é modelada através de elementos discretos: faces triangulares definidas pelos seus vértices. O Surface Evolver evolui a superfície no sentido de minimizá-la através do método da descida do gradiente (Gradient Descent Method). Esse software foi desenvolvido no Geometry Center na Universidade do Minnesota por Kenneth Brakke [[8]](https://en.wikipedia.org/wiki/Surface_Evolver).

<img src="images/SE_drop.jpg" alt="SE_drop" width=50% />

Nesse projeto, o estudante irá aprender como utilizar o Surface Evolver para estudar a forma de uma gota em um substrato, e investigar a influência de heterogeneidades. 

### Objetivos específicos do estudante

- Treinamento com o software gratuito Surface Evolver;
- Treinamento para o uso de ferramentas colaborativas: Git, GitLab;
- Treinamento de Python para análise de dados;
- Desenvolvimento de simulações em geometria simples de um fluido em contato com um sólido para entender o software e suas capacidades;
- Introduzir heterogeneidades isoladas na simulação e medir a forma e a energia do sistema;
- Explorar situações mais complexas – Cluster de heterogeneidades, rede de heterogeneidades com várias simetrias e orientações;
- Elaboração de relatórios, apresentações científicas e um pôster associado ao projeto;
- Participação em eventos científicos no Brasil;

### Resultados específicos do estudante

- Compreender a física do molhamento e estar atualizado com a literatura atual sobre o tema;
- Ter um bom domínio e controle do software Surface Evolver;
- Desenvolver simulações para obter a forma de equilíbrio da frente de molhamento para geometrias simples e complexas;
- Ser capaz de apresentar resultados científicos em apresentações e eventos científicos ;

## Bolsa 2 : Investigação experimental de fenômenos da capilaridade em um ambiente criativo

<img src="images/phenomenamosquito.png" alt="phenomenamosquito" width=37% /><img src="images/PinchedDrop.PNG" alt="PinchedDrop" width=33% />

Nesse projeto, o estudante irá desenvolver setups experimentais para estudar fenômenos de molhamento. Caso o acesso a um laboratório experimental seja limitado, o estudante poderá utilizar ferramentas como um celular e um computador para gravar e analisar a dinâmica de efeitos capilares simples. Quando o acesso a um laboratório for garantido, o estudante trabalhará no ambiente criativo IHAC-Labi [[9](http://www.ihac.ufba.br/project/ihaclab-i/), [10](https://en.wikipedia.org/wiki/Hackerspace)], onde poderá treinar habilidades da fabricação digital como: impressão 3D, Utilização de máquina de corte a Laser, etc.

### Objetivos específicos do estudante

- Treinamento para o uso de ferramentas colaborativas: Git, GitLab;
- Treinamento de fabricação digital (AutoCad, Impressão 3D, Máquina de corte a laser) no IHAC/Lab-i;
- Treinamento de Python para análise de dados;
- Projetar e construir um setup experimental para investigar fenômenos da capilaridade;
- Elaboração de relatórios, apresentações científicas e um pôster associado ao projeto;
- Participação em eventos científicos no Brasil;

### Resultados específicos do estudante

- Compreender a física do molhamento e estar atualizado com a literatura atual sobre o tema;
- Adquirir um domínio dos métodos utilizados na fabricação digital;
- Saber projetar um experimento para responder uma questão científica;
- Ser capaz de apresentar resultados científicos em apresentações e eventos científicos;

#### Referências

- [1] De Gennes, P. G.; Brochard-Wyart, F.; Quéré, D. (2004) *Capillarity and Wetting Phenomena: Drops, Bubbles, Pearls, Waves*.  Springer-Verlag.
- [2] Israelachvili, J. N. (2011). *Intermolecular and surface forces*. Academic press.
- [3] https://en.wikipedia.org/wiki/Biofouling
- [4] https://en.wikipedia.org/wiki/Ultrahydrophobicity
- [5] https://en.wikipedia.org/wiki/Dressing_(medical)
- [6] https://en.wikipedia.org/wiki/Atmospheric_water_generator
- [7] https://en.wikipedia.org/wiki/Nanoelectromechanical_systems
- [8] https://en.wikipedia.org/wiki/Surface_Evolver
- [9]  https://en.wikipedia.org/wiki/Hackerspace
- [10] http://www.ihac.ufba.br/project/ihaclab-i/