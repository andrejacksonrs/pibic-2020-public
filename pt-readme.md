# Iniciação Cientifica : Molhamento e Capilaridade

Quando dois materiais estão em contato, entre eles há uma área limitada por um determinado contorno. Esse contorno é conhecido como linha de contato. O molhamento está relacionado com a dinâmica da área de contato entre um fluido e um sólido [1] (Veja Fig. 1), enquanto a adesão lida com dois sólidos ligados através de interações intermoleculares atrativas [2].



When two materials meet, they form a contact area bounded by a contact line. Wetting is related to the dynamics of contact area between a fluid and a solid [1] (see Fig.1), while adhesion deals with two solids bonded by attractive intermolecular interactions [2]. 

![Contactline](contactline.png)

###### Figura 1 - (a) O molhamento de superfícies pode ser entendido como um problema onde a linha de contato se propaga em um potencial heterogênio (Aqui um potencial 2D harmônico). (b) Vista superior da frente da área de molhamento e da linha de contato do item (a).



###### Figure 1 (a) Wetting of heterogeneous surfaces can be cast into the problem of a contal line propagating in a heterogeneous potential (here a 2D harmonic potential). (b) Schematic of a front with a kink.

As propriedades de adesão e molhamento de materiais moles (desde fluidos simples até géis macios e elastômeros) são relevantes para uma gama enorme de aplicações incluindo antibiofouling antibiofouling [[3]](https://en.wikipedia.org/wiki/Biofouling) e revestimentos hidrofóbicos [[4]](https://en.wikipedia.org/wiki/Ultrahydrophobicity), roupas médicas [[5]](https://en.wikipedia.org/wiki/Dressing_(medical)), coletores de água [[6]](https://en.wikipedia.org/wiki/Atmospheric_water_generator), e sistemas nano-eletromecânicos [[7]](https://en.wikipedia.org/wiki/Nanoelectromechanical_systems). A linha de contato pode ser submetida a mecanismos complexos de dissipação em regime permanente e através de instabilidades (Ver Fig. 2) incluindo stick-slip (que é um fenômeno associado a quando um objeto "escapole" após uma fricção), avalanches, cavitação, pearling (fenômeno associado a geração de bolhas em aquários, por exemplo). Muitas questões em aberto aparecem quando se trata de processos dissipativos em regimes dinâmicos (regimes com o sistema em movimento).




The wetting and adhesion properties of soft materials (from simple fluids to soft gels and elastomers) are relevant for a wide range of applications including antibiofouling [[3]](https://en.wikipedia.org/wiki/Biofouling) and hydrophobic coatings [[4]](https://en.wikipedia.org/wiki/Ultrahydrophobicity), medical dressing [[5]](https://en.wikipedia.org/wiki/Dressing_(medical)), water harvester [[6]](https://en.wikipedia.org/wiki/Atmospheric_water_generator), nano-electromechanical systems [[7]](https://en.wikipedia.org/wiki/Nanoelectromechanical_systems). The contact line can be subjected to complex dissipation mechanisms in steady state motion and  through instabilities  (see Fig.2) including stick-slip, avalanches, cavitation, pearling. Many open questions arise concerning the local dissipative processes operating in the dynamical regimes.

- [1] De Gennes, P. G.; Brochard-Wyart, F.; Quéré, D. (2004) *Capillarity and Wetting Phenomena: Drops, Bubbles, Pearls, Waves*.  Springer-Verlag.
- [2] Israelachvili, J. N. (2011). *Intermolecular and surface forces*. Academic press.
- [3] https://en.wikipedia.org/wiki/Biofouling
- [4] https://en.wikipedia.org/wiki/Ultrahydrophobicity
- [5] https://en.wikipedia.org/wiki/Dressing_(medical)
- [6] https://en.wikipedia.org/wiki/Atmospheric_water_generator
- [7] https://en.wikipedia.org/wiki/Nanoelectromechanical_systems



## Bolsa 1 : Molhamento em superfícies texturizadas: Simulação com Surface Evolver

### Objetivos específicos do estudante

- Treinamento com o software gratuito Surface Evolver;
- Treinamento para o uso de ferramentas colaborativas: Git, GitLab;
- Treinamento de Python para análise de dados;
- Desenvolvendo simulações em geometria simples de um fluido em contato com um sólido para entender o software e suas capacidades;
- Introduzir uma única heterogeneidade na simulação e medir a forma e a energia associadas;
- Explorar situações mais complexas – Cluster de heterogeneidades, rede de heterogeneidades com várias simetrias e orientações;
- Preparação de relatórios, apresentações científicas e um pôster associado ao projeto;
- Participação em eventos científicos no Brasil;

### Resultados específicos do estudante

- Compreender a física do molhamento e estar em dia com a literatura atual sobre o tema;
- Ter um bom domínio e controle do software Surface Evolver;
- Desenvolver simulações para obter a forma de equilíbrio da frente de molhamento para geometrias simples e complexas;
- Ser capaz de apresentar resultados científicos em apresentações e eventos científicos ;





![SchematicCNPq](dipcoating.svg)

###### Figura 2 (a) Esquema do experimento de revestimento por imersão usando um substrato texturizado com heterogeneidades químicas. (b-e) As técnicas de litografia permitem um controle preciso do tamanho e distribuição das heterogeneidades. Cristal perfeito (b), com ruído (c), com inclusões (d), com contorno de grão (e).





## Bolsa 2 : Investigação experimental de fenômenos da capilaridade em um espaço de criação

### Objetivos específicos do estudante

- Treinamento para o uso de ferramentas colaborativas: Git, GitLab;
- Treinamento de fabricação digital (AutoCad, Impressão 3D, Máquina de corte a laser) in the HIAC/Lab-i;
- Treinamento de Python para análise de dados;
- Projetar e construir um setup experimental para investigar fenômenos da capilaridade;
- Prepare reports, scientific communcations, poster associated with the present project;
- Preparação de relatórios, apresentações científicas e um pôster associado ao projeto;
- Participação em eventos científicos no Brasil;

### Resultados específicos do estudante

- Understand the physics of wetting and be up to date with the current litterature (?)
- Have a good master of control of the software Surface Evolver (?)
- Develop simulations to obtain the equilibrium of wetting front in simple and more complex geometry (?)
- Ser capaz de apresentar resultados científicos em apresentações e eventos científicos ;


